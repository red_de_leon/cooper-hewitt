/**
 *  GraphViz.js
 *
 *
 */

import { TextureLoader, SpriteMaterial, Sprite } from 'three';
import ForceGraph3D from '../lib/3d-force-graph/3d-force-graph';
import SpriteText from '../lib/three-spritetext/three-spritetext';
import axios from 'axios';
import _ from 'lodash';
import { ColorPalette } from './ColorPalette';

const INIT = {
        fontSizes: {
          artwork: 50,
          participant: 50,
          country: 100
        },
        force: {
          strength: -500
        },
        graphID: 'graph',
        edgeLength: {
          artwork: 40,
          participant: 40,
          participantLink: 40,
          countryLink: 40,
          countryCountryLink: 100,
          artworkArtworkLink: 40,
          artworkCountryLink: 200,
          artworkYearLink: 40
        },
        imageDir: 'lamps',
        nodeVal: 60,
        zoom: 2500,
        zoomTarget: 20000,
        zoomIncrement: 70,
        zoomDelay: 2000
      },
      $graph = document.getElementById(INIT.graphID),
      linkColors = {
        artwork:             ColorPalette.white,
        participant:         ColorPalette.orange,
        participantLink:     ColorPalette.orange,
        countryLink:         ColorPalette.aqua,
        countryCountryLink:  ColorPalette.gold,
        artworkArtworkLink:  ColorPalette.gold,
        artworkYearLink:     ColorPalette.orange,
        artworkCountryLink:  ColorPalette.aqua,
      }, 
      textColors = {
        participant:         ColorPalette.orange,
        artwork:             ColorPalette.white,
        participantLink:     ColorPalette.orange,
        country:             ColorPalette.white,
        year:                ColorPalette.orange,
      };

let $detail = {},
    $panelCloseButton = null,
    $panelRight = null,
    allowRotation = true,
    dataTimestamp = '',
    graph = null,
    graphData = {
      nodes: [],
      links: []
    },
    introZoom = false,
    labelReferences = [],
    nodeCount = 0,
    sourceJSON = '',
    sourceJSONSubset = '',
    spinning = true,
    timeStart = null;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function GraphViz() {
  // Pre-prep
  const docBody = document.getElementById('body');
  sourceJSON = docBody.dataset.sourceJson;
  sourceJSONSubset = docBody.dataset.sourceJsonSubset;
  dataTimestamp = docBody.dataset.dataTimestamp;
  // Prep
  prepareTemplates();
  fetchData();
  timeStart = Date.now();
}

function onWindowResize() {
  graph
    .width(window.innerWidth * 1)
    .height(window.innerHeight);
}

function getDOMByClass(className) {
  return document.getElementsByClassName(className)[0];
}

function buildGraph(graphData) {
  graph = ForceGraph3D()
    (document.getElementById(INIT.graphID))
      .backgroundColor('rgba(0,0,0,0)')
      .dagMode(null)
      /*.dagNodeFilter(false)*/
      .graphData(graphData)
      .nodeOpacity(1)
      .nodeVal(INIT.nodeVal)
      .nodeResolution(50)
      .nodeColor(() => ColorPalette.orange)
      .linkOpacity(0.75)
      .linkColor((l) => {
        return assignColor(linkColors, l).col;
      })
      .nodeThreeObject((n) => {
        if (n.type === 'artwork') {
          return null;
        } else {
          let { col } = assignColor(textColors, n),
              sprite,
              label = n.label;
          sprite = labelReferences[n.id] = new SpriteText(label);
          sprite.fontFace = 'Abel';
          sprite.padding = 3;
          sprite.material.depthWrite = false;
          sprite.color = col;
          sprite.colorOriginal = col;
          sprite.textHeight = INIT.fontSizes[n.type];
          sprite.textHeightOriginal = INIT.fontSizes[n.type];
          sprite.name = 'spriteTextOff';
          return sprite;
        }
      })
      .onNodeHover((node, prevNode) => {
        if ($graph) {
          $graph.style.cursor = (node && (node.type === 'artwork')) ? 'pointer' : null;
        }
        if ((node !== null) &&
            (node.__threeObj) &&
            (node.__threeObj !== null) && 
            (node.type === 'artwork')) {
          node.__threeObj.color = ColorPalette.ltGray;
        }
        if ((prevNode !== null) &&
            (prevNode.__threeObj) &&
            (prevNode.__threeObj !== null)) {
          prevNode.__threeObj.color = prevNode.__threeObj.colorOriginal;
        }
      })
      .onNodeClick((node) => {
        if (node && node.type === 'participant') return;
        viewNodeDetails(node);
      });

  graph.d3Force('charge').strength(INIT.force.strength);
  graph.d3Force('link').distance((n) => {
    return INIT.edgeLength[n.type];
  });
  graph.cameraPosition({ z: INIT.zoom });
  graph.scene().rotation.y = Math.random() * 360;
  // graph.onEngineTick(inspectGraph);
  spinGraph();
  window.addEventListener('resize', onWindowResize);
  onWindowResize();
}

function prepareTemplates() {
  let wrapper = document.getElementById('detail-template-wrapper'),
      templateHTML = document.getElementById('detail-template').innerHTML;
  wrapper.innerHTML = templateHTML;
  $panelRight = getDOMByClass('panel--right');
  $detail = {
    $image: getDOMByClass('detail__item--image').getElementsByTagName('img')[0],
    $title: getDOMByClass('detail__title').getElementsByTagName('a')[0],
    $year: getDOMByClass('detail__item--year').getElementsByTagName('dd')[0],
    $participants: getDOMByClass('detail__item--participant').getElementsByTagName('dd')[0],
    $medium: getDOMByClass('detail__item--medium').getElementsByTagName('dd')[0],
    $country: getDOMByClass('detail__item--country').getElementsByTagName('dd')[0],
  };
  $panelCloseButton = getDOMByClass('button--close');
  $panelCloseButton.addEventListener('click', onPanelClose);
}

function viewNodeDetails(node) {
  let imagePath;
  if (!node) {
    $panelRight.classList.add("hidden");
    allowRotation = true;
  } else if (node.type === 'artwork') {
    allowRotation = false;
    // Clear
    $detail.$image.setAttribute('src', '');
    $detail.$image.setAttribute('alt', '');
    $detail.$title.innerHTML = '';
    $detail.$year.innerHTML = '';
    $detail.$participants.innerHTML = '';
    $detail.$title.setAttribute('href', '');
    $detail.$medium.innerHTML = '';
    $detail.$country.innerHTML = '';
    // Update
    imagePath = './img/' + INIT.imageDir + '/' + node.object_id + '_b_0.jpg';
    $panelRight.classList.remove("hidden");
    $detail.$image.setAttribute('alt', node.label);
    $detail.$image.setAttribute('src', imagePath);
    $detail.$title.innerHTML = node.label;
    $detail.$title.setAttribute('href', (node.url) ? node.url : '');
    $detail.$year.innerHTML = node.date;
    $detail.$participants.innerHTML = (node.participants.map((p) => { return p.person_name })).join(', ');
    $detail.$medium.innerHTML = node.medium;
    $detail.$country.innerHTML = node.country;
  } else {
    $panelRight.classList.add("hidden");
    allowRotation = true;
  }
}

function onPanelClose() {
  viewNodeDetails(null);
}

function assignColor(type, node) {
  let col = type[node.type];
  return { col };
}

// function inspectGraph() {
// }

function spinGraph() {
  if (allowRotation === true) {
    graph.scene().rotation.y += 0.0005;
  }
  if ((Date.now() - timeStart) > INIT.zoomDelay) {
    if ((introZoom === true) &&
        (graph.cameraPosition().z < INIT.zoomTarget)) {
      let newZ = graph.cameraPosition().z += INIT.zoomIncrement;
      graph.cameraPosition({ z: newZ });
    } else {
      introZoom = false;
    }
  }
  if (spinning) {
    requestAnimationFrame(spinGraph);
  }
}

function isIOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  // iPad on iOS 13 detection
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

function fetchData() {
  const sourceData = (isIOS() === true) ? sourceJSONSubset : sourceJSON;
  axios
    .get('./data/' + sourceData + '?' + dataTimestamp)
    .then(data => {
      // nodeCount = data.data.length;
      // console.log('nodeCount: ' + nodeCount);
      setTimeout(() => { processData(data.data.objects) }, 200);
    })
    .catch(error => {
      console.log(error);
    });
}

function getYearNodes(source) {
  let temp = [],
      uniqueYears,
      uniqueYearsReformat,
      combinedList = [];
  _.each(source, (y) => { combinedList.push(y.date); });
  uniqueYears = combinedList.filter(function(item) {
    if (temp[item]) return false;
    temp[item] = true;
    return true;
  });
  uniqueYears = uniqueYears.sort((a, b) => {
    return (a > b) ? 1 : -1; 
  });
  uniqueYearsReformat = uniqueYears.map((item, i) => {
    return { 
      index: i,
      id: 'y' + item,
      label: item,
      type: 'year'
    };
  });
  return [...uniqueYearsReformat];
}

function getArtworkCountryLinks(sourceA, sourceB) {
  let links = [],
      artwork,
      artworks = [...sourceA],
      countries = [...sourceB];
  for (let i = 0; i < artworks.length; i++) {
    artwork = artworks[i];
    if (artwork.countryID) {
      for (let k = 0; k < countries.length; k++) {
        if (artwork.countryID === countries[k].id) {
          links.push({
            source: artwork.id,
            target: countries[k].id,
            type: 'artworkCountryLink',
            value: 10
          });
        }
      }
    }
  }
  return [...links];
}

function getArtworkYearLinks(sourceA, sourceB) {
  let links = [],
      artwork,
      artworks = [...sourceA],
      years = [...sourceB];
  for (let i = 0; i < artworks.length; i++) {
    artwork = artworks[i];
    if (artwork.date) {
      for (let k = 0; k < years.length; k++) {
        if (artwork.date === years[k].id) {
          links.push({
            source: artwork.id,
            target: years[k].id,
            type: 'artworkYearLink',
            value: 10
          });
        }
      }
    }
  }
  return [...links];
}

function getCountryCountryLinks(source) {
  let links = [],
      items = [...source];
  _.each(items, (item, i) => {
    if (i < (items.length - 1)) {
      links.push({
        source: item.id,
        target: items[i + 1].id,
        type: 'countryCountryLink',
        value: 10
      });
    }
  });
  return [...links];
}

function getArtworkArtworkLinks(source) {
  let links = [],
      items = [...source];
  _.each(items, (item, i) => {
    if (i < (items.length - 1)) {
      links.push({
        source: item.id,
        target: items[i + 1].id,
        type: 'artworkArtworkLink',
        value: 10
      });
    }
  });
  return [...links];
}

function getParticipants(source) {
  let artworks = [...source],
      participants = [],
      temp = [],
      uniqueIDs = [],
      final = [];
  for (let i = 0; i < artworks.length; i++) {
    let artwork = artworks[i];
    if (artwork.participants) {
      for (let j = 0; j < artwork.participants.length; j++) {
        let p = artwork.participants[j];
        participants.push({
          index: i,
          id: `p${p.person_id}`,
          person_id: p.person_id,
          role_id: p.role_id,
          label: p.person_name,
          person_date: p.person_date,
          role_name: p.role_name,
          person_url: p.person_url,
          type: 'participant',
          value: 10
        });
      }
    }
  }
  uniqueIDs = participants.filter(function(item) {
    if (temp[item.person_id]) return false;
    temp[item.person_id] = true;
    return true;
  });
  for (let j = 0; j < uniqueIDs.length; j++) {
    let u = uniqueIDs[j];
    for (let i = 0; i < participants.length; i++) {
      if (u.person_id === participants[i].person_id) {
        final.push(participants[i]);
        break;
      }
    }
  }
  return [...final];
}

function getCountryNodes(source) {
  let temp = [],
      uniqueCountries,
      uniqueCountriesReformat,
      combinedList = [];

  _.each(source, (y) => { 
    if (y.country) {
      combinedList.push({
        id: y.countryID,
        label: y.country
      }); 
    }
  });
  uniqueCountries = combinedList.filter(function(item) {
    if (temp[item.id]) return false;
    temp[item.id] = true;
    return true;
  });
  uniqueCountries = uniqueCountries.sort((a, b) => {
    return (a.label > b.label) ? 1 : -1; 
  });
  uniqueCountriesReformat = uniqueCountries.map((item) => {
    return { 
      id: item.id,
      label: item.label,
      type: 'country'
    };
  });
  return [...uniqueCountriesReformat];
}

function getCountryName(item) {
  let c = {};
  if (item['woe:country']) {
    c.countryID = 'c' + item['woe:country'];
  } else {
    c.countryID = 'c_';
  }
  if (item['woe:country_name']) {
    c.country = item['woe:country_name'];
  } else {
    c.country = '?';
  }
  return c;
}

function processData(source) {
  let convertedItems = [],
      countryNodes = [],
      originalItems = [...source],
      participantNodes = [],
      yearNodes;
  convertedItems = originalItems.map((item, i) => ({
    index: i,
    id: `a${item.id}`,
    object_id: item.id,
    date: item.date,
    label: item.title,
    type: 'artwork',
    medium: item.medium,
    participants: item.participants,
    url: item.url,
    value: 10,
    countryID: (getCountryName(item)).countryID,
    country: (getCountryName(item)).country
  }));
  convertedItems = convertedItems.sort(
    (a, b) => (a.id > b.id) ? 1 : -1
  );

  graphData.nodes = [...convertedItems];
  participantNodes = getParticipants(convertedItems);
  countryNodes = getCountryNodes(convertedItems);
  yearNodes = getYearNodes(convertedItems);

  graphData.links = getArtworkCountryLinks (
    graphData.nodes,
    countryNodes
  );

  // graphData.links = graphData.links.concat(
  //   getArtworkArtworkLinks(graphData.nodes)
  // );

  graphData.links = graphData.links.concat(
    getCountryCountryLinks(countryNodes)
  );

  graphData.nodes = [
    ...graphData.nodes,
    ...countryNodes
  ];

  // console.log(graphData.nodes);

  buildGraph(graphData);
  nodeCount = convertedItems.length;
  console.log('nodeCount: ' + nodeCount);
}

export default GraphViz;
