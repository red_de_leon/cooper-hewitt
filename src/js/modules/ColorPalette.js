/**
 *   ColorPalette.js
 *
 *
 */

export const ColorPalette = {
  white:      '#ffffff',
  black:      '#000000',
  dkLavender: '#6C5A97',
  orange:     '#FF5700',
  pink:       '#ad0062',
  ltGray:     '#aaaaaa',
  aqua:       '#7adfb5',
  gold:       '#fcc201'
};
