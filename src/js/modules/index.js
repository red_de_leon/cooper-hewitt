/**
 *  index.js
 *
 *
 */

import GraphViz from './GraphViz';
import './index.scss';
import './button.scss';
import './detail.scss';
import './panel--left.scss';
import './panel--right.scss';
import './status.scss';
import './alt-content.scss';

const graphViz = GraphViz();

