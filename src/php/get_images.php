<?php

/**
 *  This script parses Cooper Hewitt JSON files
 *  and downloads images from their servers
 *  and copies them in the target directory.
 *  
 *
 */

$dir_name = "lamps";
$json_file_contents = file_get_contents("../data/lamps.json");
$json = json_decode($json_file_contents, true);
$objects = $json['objects'];
$counter = 0;
$x_found = false;
for ($i = 0; $i < count($objects); $i++) {
  usleep(200);
  $object_id = $objects[$i]["id"];
  $counter++;
  /*echo '<h3>'.$object_id.'</h3>';*/
  /*if ($i >= 0) {*/
    for ($j = 0; $j < count($objects[$i]['images']); $j++) {
      /*echo '<ul>';*/
      foreach ($objects[$i]['images'][$j] as $key => $value) {
        if ($key == 'b') {
          $x_found = true;
          $the_url = $value["url"];
          $the_url = str_replace("https", "http", $the_url);
          $new_file_name = $dir_name."/".$object_id."_".$key."_".$j.".jpg";
          // Uncomment next two lines:
          // $image_response = file_get_contents($the_url);
          // file_put_contents($new_file_name, $image_response);
          /*echo '<li><strong>'.$key.'</strong>: '.$value["url"].'</li>';*/
        }
      }
      /*echo '</ul>';*/
    }
  /*}*/
  
}
/*echo $counter . " objects found";*/
?>
<p>Done.</p>
