/**
 *  webpack.config.js
 *
 *
 */

const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLBeautifyWebpackPlugin = require('beautify-html-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const _ = require('lodash');

const INIT = {
  appTitle: 'Lamp Design',
  appDescription: 'A graph visualization of lamps from the Cooper Hewitt design collection',
  sourceJSON: 'lamps.json',
  sourceJSONSubset: 'lamps.json',
  dataTimestamp: 202103120
};

const INIT2 = {
  appTitle: 'Chair Design',
  appDescription: 'A graph visualization of chairs from the Cooper Hewitt design collection',
  sourceJSON: 'chairs.json',
  sourceJSONSubset: 'chairs.json',
  dataTimestamp: 202103103
};

module.exports = {
  entry: path.resolve(__dirname, './src/js/modules/index.js'),
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(s*)css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                return path.relative(path.dirname(resourcePath), context) + '/';
              },
            }
          },
          'css-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.twig$/,
        use: [
          'raw-loader',
          {
            loader: 'twig-html-loader',
            options: {
              data: (context) => {
                const data = path.join(
                  __dirname,
                  './src/data/' + INIT.sourceJSON
                );
                context.addDependency(data);
                let items = (
                  context.fs.readJsonSync(data, { throws: false })
                 ).objects;
                _.each(items, (item) => {
                  item.country_name = item['woe:country_name'];
                  item.participantsJoined = (item.participants.map((p) => { return p.person_name })).join(', ')||'?';
                  item.cleanDate = item.date||'?';
                  item.medium = item.medium||'?';
                });
                items = items.sort(
                  (a, b) => (a.title > b.title) ? 1 : -1
                );
                const app = {
                  title:            INIT.appTitle,
                  description:      INIT.appDescription,
                  sourceJSON:       INIT.sourceJSON,
                  sourceJSONSubset: INIT.sourceJSONSubset,
                  dataTimestamp:    INIT.dataTimestamp
                };
                return { app, items } || {};
              }
            }
          },
        ]
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      protectWebpackAssets: true,
      cleanStaleWebpackAssets: false,
      cleanOnceBeforeBuildPatterns: [
        'img/*'
      ],
      cleanAfterEveryBuildPatterns: [
        'css/*',
        'js/*'
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './src/data/*.json',
          to: 'data/[name].[ext]',
        }
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/img',
          to: 'img',
        }
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/favicon/*.*',
          to: './[name].[ext]',
        }
      ],
    }),
    new MiniCssExtractPlugin({
      filename: 'css/index.[contenthash].css'
    }),
    new HTMLWebpackPlugin({
      inject: 'body',
      template: 'src/twig/index.twig',
      filename: 'index.html'
    }),
    new HTMLBeautifyWebpackPlugin({
      options: {
        end_with_newline: true,
        indent_size: 2,
        indent_with_tabs: false,
        indent_inner_html: true,
        preserve_newlines: true,
        unformatted: ['p', 'i', 'b', 'span']
      }
    }),
  ],
  resolve: {
    extensions: ['*', '.js']
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'js/index.[contenthash].js',
  },
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
  },
};