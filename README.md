cooper-hewitt
=============

## Overview

This is a project exploring the Cooper Hewitt collection database, showcasing their chair design collection.

A live version of this site can be viewed here: [chair-design.red-deleon.com](https://chair-design.red-deleon.com/)

## Sips image processing reference

    ```
    for i in *.jpg; do sips -s format jpeg -s formatOptions 80 "${i}" --out "./done/${i%.jpg}.jpg"; done
    ```